
Deployment notifications
------------------------
Setup deployment-notifications by adding the following to the sites context:
drush provision-save @site.alias --context-type=newrelic --nr_report_deploy_enabled=<true | false>
drush provision-save @site.alias --context-type=newrelic --nr_vhost_appname="uri,profile"
drush provision-save @site.alias --context-type=newrelic --nr_api_key=<api-key from New Relic RPM>
drush provision-save @site.alias --context-type=newrelic --nr_app_id=<7-digit numeric id, not required if app_name is set>
drush provision-save @site.alias --context-type=newrelic --nr_app_name=<app-name, not required if app_id is set>

You can also set several properties in one go, the following enables reporting for a site which will be named by its uri in RPM and identified via app-id (replace api-key and app-id)
drush provision-save @scleroseforeningen.dk --context-type=newrelic --nr_report_deploy_enabled=true --nr_vhost_appname=uri --nr_api_key=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --nr_app_id=xxxxxxx

Application name(s)
-------------------
By default a new section will be added to the sites virtual-host configuration
that indicates that the site is running drupal, and injects a number of extra
names for the site. The names will show up in the New Relic RPM and can be
controlled via the nr_vhost_appname parameter. You have the choice of one or
more of the following (seperate by comma if more than one value is specified).
If a site has more than one name it will show up once for each name it has:

  - profile: Use the sites install profile as its name.
  - uri: Use the URI of the site as its name.
  - aggregate: Use the static string "All Sites" as the sites name - use this if
               you want to aggregate the statictics of several sites into one.
  - all: (default) Use all of the above.

drush provision-save @site.alias --context-type=newrelic --nr_vhost_appname=<value(s)>
