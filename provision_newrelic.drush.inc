<?php

/**
 * @file
 * Hookup with drush
 */

define('PROVISION_NEWRELIC_DEPLOY_URL', 'https://rpm.newrelic.com/deployments.xml');
include_once 'provision_newrelic_license.context.service.inc';

/**
 * Implements hook_provision_apache_vhost_config().
 */
function provision_newrelic_provision_apache_vhost_config($data = NULL) {
  $config = new provisionConfig_drushrc_site(d()->name);

  // Determine which app-names to inject (if any).
  $variations = array(
    'aggregate' => 'All Sites',
    'profile' => 'Profile - ' . $config->profile,
    'uri' => $config->uri,
  );
  $appnames = array();
  foreach ($variations as $needle => $name) {
    if (strpos(d()->nr_vhost_appname, $needle) !== FALSE ||
      d()->nr_vhost_appname == 'all'
    ) {
      $appnames[] = $name;
    }
  }
  $appnames = implode('; ', $appnames);
  drush_log(dt("Adding application name !appnames to vhost configuration",
    array('!appnames' => $appnames))
  );

  // Prepare the config and return it.
  if (!empty($appnames)) {
    $appnames_config .= 'php_value newrelic.appname "' . $appnames . '"';
  }
  $config = <<<EOT
  <IfModule php5_module>
    $appnames_config
    php_value newrelic.framework "drupal"
    php_value newrelic.framework.drupal.modules 1
  </IfModule>
EOT;

  return $config;
}

/**
 * Implements hook_drush_command().
 */
function provision_newrelic_drush_command() {
  $items = array();

  $items['provision-newrelic-deploy'] = array(
    'description' => "Reports the start of a deployment to New Relic. You must as a minimum specify either app_name or application_id, or store one of these two configurations into your sites context via provision-save. Both values can be found in the New Relic RPM administration.",
    'aliases' => array('nrdep'),
    'options' => array(
      'app_name' => array(
        'description' => 'The value of app_name in the newrelic.yml file used by the application. This may be different than the label that appears in the RPM UI. You can find the app_name value in RPM by looking at the label settings for your application.',
      ),
      'application_id' => array(
        'description' => 'The numeric ID of the application, can be found in the URL\'s in New Relics RPM Dashboard.',
      ),
      'changelog' => array(
        'description' => 'A list of changes for this deployment.',
      ),
      'description' => array(
        'description' => 'Text annotation for the deployment.',
      ),
      'user' => array(
        'description' => 'The user executing the deployment',
      ),
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  return $items;
}

/**
 * Implements drush_hook_COMMAND().
 */
function drush_provision_newrelic_deploy() {
  $context = d();
  $parameters = array (
    'app_name' => '', // site or   sitename
    'application_id' => '', //site
    'description' => '', // runtime
    'revision' => '', // runtime / git / platform?
    'changelog' => '', // Runtime
    'user' => '', // Runtime
  );

  provision_newrelic_deploy($parameters, $apikey);
}

/**
 * Validation-hook for provision_newrelic_deploy.
 *
 * Checks that either the app_anme or application_id is set.
 */
function drush_provision_newrelic_deploy_validate($parameters, $api_key) {
  if (drush_get_option('app_name', FALSE) === FALSE
    && drush_get_option('application_id', FALSE) === FALSE) {
    return drush_set_error('MISSING_APP_IDENT', dt('Either app_name or application_id must be specified'));
  }

  return TRUE;
}

/**
 * Reports a deployment to New Relic.
 *
 * See http://newrelic.github.io/newrelic_api/NewRelicApi/Deployment.html for
 * API details.
 *
 * @param string $parameters
 *   parameters to be sent to New Relic.
 * @param string $api_key
 *   The API-key for the New Relic account under which the application is
 *   deployed
 *
 * @return bool
 *   TRUE on success, FALSE on failure.
 */
function provision_newrelic_deploy($parameters, $api_key) {
  // Setup HTTP headers and body.
  $options = array(
    'http' => array(
      'header'  => "Content-type: application/x-www-form-urlencoded\r\n" . "x-api-key:$api_key\r\n",
      'method'  => 'POST',
      'content' => http_build_query($parameters),
    ),
  );
  $context  = stream_context_create($options);

  // Attempt to report in the deployment.
  $result = file_get_contents(PROVISION_NEWRELIC_DEPLOY_URL, FALSE, $context);

  // Get the status header, potentially file_get_contents goes trough a number
  // of redirects, but its the first Status: we want so no problem in fetching
  // out the header in the following way.
  if (isset($http_response_header)) {
    foreach ($http_response_header as $header) {
      if (preg_match("#^Status: (.*)#", $header, $matches)) {
        drush_log(dt('New Relic REST API response:: !response', array('!response' => $matches[1]), 'debug'));
        break;
      }
    }
  }

  // Output the result.
  if ($result !== FALSE) {
    // TODO: Get the deployment ID from the first Location: header.
    return TRUE;
  }
  else {
    drush_log(dt('Could not report deployment to New Relic'), 'notice');
    return TRUE;
  }
}

/**
 * Implements drush_hook_pre_COMMAND().
 */
function drush_provision_newrelic_pre_provision_migrate($platform, $new_uri = NULL) {
  // Should be a site-context.
  $context = d();

  if ($context->type !== 'site') {
    drush_log(dt('Someone attempted an migration of something that was not a site, not reported to New Relic'), 'notice');
    return;
  }

  if (d($platform)->type !== 'platform') {
    drush_log(dt('Someone attempted an migration to something that was not a platform, not reported to New Relic'), 'notice');
    return;
  }

  if ($context->nr_report_deploy_enabled === FALSE || strtolower($context->nr_report_deploy_enabled) == 'false') {
    // Reporting disabled.
    return;
  }

  // Make sure either app_name or app_id is set.
  if (!(isset($context->nr_app_id) && !isset($context->nr_app_name))) {
    drush_log(dt('Could not report deployment: either application-id (nr_app_id) or application-name (nr_app_name) must be set'), 'error');
    return;
  }

  if (!isset($context->nr_api_key)) {
    drush_log(dt('Could not report deployment: api-key (nr_api_key) has not been set.'), 'error');
    return;
  }

  $parameters = array();

  $apikey = $context->nr_api_key;

  if (isset($context->nr_app_id)) {
    $parameters['application_id'] = $context->nr_app_id;
  }

  if (isset($context->nr_app_name)) {
    $parameters['app_name'] = $context->nr_app_name;
  }

  // Lacking any better name we use the platforms directory as a name.
  $source_platform_name = basename($context->platform->root);
  $destination_platform_name = basename(d($platform)->root);

  // Generate description.
  $parameters['description'] = dt('Migrating the site !site from platform !source to !destination',
    array(
      '!site' => $context->uri,
      '!source' => $source_platform_name,
      '!destination' => $destination_platform_name,
    )
  );
  $parameters['revision'] = $destination_platform_name;

  if (provision_newrelic_deploy($parameters, $apikey)) {
    drush_log(dt('Reported migration of !site to New Relic', array('!site' => $context->uri)), 'ok');
  }
  else {
    drush_log(dt('Could not report deployment to New Relic'), 'error');
  }
}
