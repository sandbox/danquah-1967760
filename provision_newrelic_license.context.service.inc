<?php
/**
 * @file
 * Contexts used in Git Project.
 */

/**
 * Expose the service type this extension defines to provision.
 *
 * @return array
 *   An array with the service type the key, and the default implementation the
 *   value.
 */
function provision_newrelic_provision_services() {
  return array(
    'newrelic' => NULL,
  );
}

/**
 * A service that stores the various New Relic related properties.
 */
class provisionService_newrelic extends provisionService {
  public $service = 'newrelic';

  static function subscribe_site(provisionContext_site $context) {
    // Add our properties to the context.
    $context->setProperty('nr_app_name');
    $context->setProperty('nr_app_id');
    $context->setProperty('nr_api_key');
    $context->setProperty('nr_report_deploy_enabled', FALSE);

    // Vhost configuration, allowed values:
    // aggregate: Static "All Sites" string.
    // profile: The profile of the site.
    // uri: The URI of the site.
    // all: All of the above.
    $context->setProperty('nr_vhost_appname', 'all');
  }
}

